<?php
	
	// @author Himanshu Choudhary
	// @email himanshuchoudhary@live.com
	// @git https://bitbucket.org/himanshuchoudhary/

	// error_reporting(-1);
	// ini_set('display_errors', 'On');

	include 'class.cache.php';

	if($_SERVER['REQUEST_METHOD']=='POST'){
		$operandA = (double)$_POST['operandA'];
		$operandB = (double)$_POST['operandB'];
		$operator = $_POST['operator'];
		
		if ($operandA > $operandB && ($operator == '+' || $operator == '*')) {
			$temp = $operandA;
			$operandA = $operandB;
			$operandB = $temp;
		}

		$cache = new Cache();

		$res = $cache->get($operandA,$operandB,$operator);

		if($res['found'] == TRUE){
			echo trim($res['value']);
		}
		else {
			if($operator == '+'){
				$value = $operandA+$operandB;
			}
			elseif ($operator == '-') {
				$value = $operandA-$operandB;
			}
			elseif ($operator == '*') {
				$value = $operandA*$operandB;
			}
			elseif ($operator == '/') {
				if ($operandB != 0){
					$value = $operandA/$operandB;
				}
				else {
					$value = 'NaN';
				}
			}
			else {
				$value = NULL;
			}

			if($value !== NULL) {
				$cache->set($operandA,$operandB,$operator,$value);
				echo trim($value);
			}
			else {
				echo "error";
			}
		}
	}
	else{
		echo 'error';
	}
?>