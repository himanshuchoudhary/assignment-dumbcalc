<?php

	// @author Himanshu Choudhary
	// @email himanshuchoudhary@live.com
	// @git https://bitbucket.org/himanshuchoudhary/

	Class Cache {
		
		const MAX_CACHE_SIZE = 100;

		private $cache;

		public function __construct()
		{
			$this->cache = file('cache.txt');

			if(sizeof($this->cache) > self::MAX_CACHE_SIZE){
				$this->flush();
			}
		}

		public function get($opA,$opB,$opr)
		{
			$size = sizeof($this->cache);
			$key = $opA.$opr.$opB;

			for($i = 0; $i < $size; $i++){
				$row = explode('=',$this->cache[$i]);
				if ($row[0] == $key){
					return array(
						'found' => TRUE,
						'value' => $row[1]
					);
				}
			}

			return array(
				'found' => FALSE,
				'value' => NULL
			);
		}

		public function set($opA,$opB,$opr,$val)
		{
			$row = $opA.$opr.$opB.'='.$val.PHP_EOL;
			file_put_contents('cache.txt', $row , FILE_APPEND);
		}

		private function flush() 
		{
			file_put_contents('cache.txt', "");
		}
	}
?>