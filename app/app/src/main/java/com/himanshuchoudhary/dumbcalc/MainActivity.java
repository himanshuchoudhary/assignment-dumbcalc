package com.himanshuchoudhary.dumbcalc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private EditText inp_operandA;
    private EditText inp_operandB;
    private TextView txt_exp;
    private TextView txt_res;
    private TextView txt_msg;
    private Button btn_add;
    private Button btn_sub;
    private Button btn_mul;
    private Button btn_div;
    private Button btn_cal;
    private Button btn_reset;
    private Button btn_swap;

    private Animation translate;
    private String val_operator = " ";
    private static final String REQUEST_URL = "http://home.iitk.ac.in/~himnshu/cs654a/dumbcalc/compute.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inp_operandA = (EditText) findViewById(R.id.inpOperandA);
        inp_operandB = (EditText) findViewById(R.id.inpOperandB);
        txt_exp = (TextView) findViewById(R.id.textExpression);
        txt_res = (TextView) findViewById(R.id.textResult);
        txt_msg = (TextView) findViewById(R.id.textMsg);
        btn_add = (Button) findViewById(R.id.btnOperatorAdd);
        btn_sub = (Button) findViewById(R.id.btnOperatorSub);
        btn_mul = (Button) findViewById(R.id.btnOperatorMul);
        btn_div = (Button) findViewById(R.id.btnOperatorDiv);
        btn_cal = (Button) findViewById(R.id.btnCalculate);
        btn_reset = (Button) findViewById(R.id.btnReset);
        btn_swap = (Button) findViewById(R.id.btnSwap);

        translate = new TranslateAnimation(0,0,50,0);
        translate.setDuration(100);
        translate.setFillEnabled(true);
        translate.setFillAfter(true);

        inp_operandA.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    updateExpression();
                }
            }
        });

        inp_operandB.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    updateExpression();
                }
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                val_operator = "+";
                updateExpression();
            }
        });

        btn_sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                val_operator = "-";
                updateExpression();
            }
        });

        btn_mul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                val_operator = "*";
                updateExpression();
            }
        });

        btn_div.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                val_operator = "/";
                updateExpression();
            }
        });

        btn_cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateExpression();
                if(inp_operandA.getText().toString().matches("")){
                    txt_msg.setText("Please enter operand A.");
                    return;
                }
                if(inp_operandB.getText().toString().matches("")){
                    txt_msg.setText("Please enter operand B.");
                    return;
                }
                if(val_operator == " "){
                    txt_msg.setText("Please select some operator.");
                    return;
                }
                getResult();
            }
        });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inp_operandA.setText("");
                inp_operandB.setText("");
                val_operator = "";
                updateExpression();
            }
        });

        btn_swap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp;
                temp = inp_operandB.getText().toString();
                inp_operandB.setText(inp_operandA.getText().toString());
                inp_operandA.setText(temp);
                updateExpression();
            }
        });
    }

    private void updateExpression(){
        txt_exp.setText(inp_operandA.getText().toString() + " " + val_operator + " " + inp_operandB.getText().toString());
    }

    private void getResult() {
        final String operandA = inp_operandA.getText().toString().trim();;
        final String operandB = inp_operandB.getText().toString().trim();;
        final String operator = val_operator;

        StringRequest request = new StringRequest(Request.Method.POST, REQUEST_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        txt_res.setText("= "+response);
                        txt_res.startAnimation(translate);
                        txt_msg.setText("");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        txt_msg.setText(error.toString());
                    }
                }
        ){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("operandA", operandA);
                params.put("operandB", operandB);
                params.put("operator", operator);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request);
    }
}
